/*
******************************************************************************
File:     main.c
Info:     Generated by Atollic TrueSTUDIO(R) 6.0.0   2016-09-27

The MIT License (MIT)
Copyright (c) 2009-2016 Atollic AB

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
******************************************************************************
*/

/* Includes */
#include "stm32f4xx.h"
#include "nixie_stm32f4xx_init.h"
#include "nixie_tubes.h"
#include "nixie_dcf77.h"
#include "nixie_vl6180x.h"

/* Private definitions */
#define STATE_NO_SYNC			0x0000		// RTC not synchronized with DCF77
#define STATE_SYNC				0xFFFF		// RTC synchronized with DCF77
#define STATE_SEARCHING_DCF77	0x0001		// RTC not synchronized, searching for DCF77 sequence
#define STATE_READING_DCF77		0x0002		// RTC not synchronized, reading DCF77 sequence
#define STATE_CHECKING_DCF77	0x0003		// RTC synchronized, checking with DCF77

#define DATE_MASK				0x00FF1F3F	// Masking year, month and day
#define TIME_MASK				0x003F7F00	// Masking hour and minute

/* Private macro */
/* Private variables */

/* Private function prototypes */
uint16_t stateSync(uint32_t aTime, uint32_t aDate);
uint16_t stateCheckingDCF77(uint32_t aTime, uint32_t aDate);
uint16_t stateSearchingDCF77(uint32_t aTime, uint32_t aDate);
uint16_t stateReadingDCF77(uint32_t aTime, uint32_t aDate);

void tubesOff(uint16_t aMethod, uint16_t aDelay);
void tubesAnimate(uint16_t aDigit, uint16_t aMethod, uint16_t aDelay);

uint16_t convTimeToDigits(uint32_t aTime);
uint16_t convDateToDigits(uint32_t aDate);
uint16_t convDCF77ToDigits(uint16_t aSyncState);

/*
**===========================================================================
**
**  Abstract: main program
**
**===========================================================================
*/

int main(void)
{
	uint16_t clockState = STATE_NO_SYNC;

	nixieInit();		// Initialize peripherals

	dcfResync();		// Initialize synchronization with DCF77

	dotsFlash(3);		// Flash dots 3 times just for fun :-)

	uint32_t newTime = RTC->TR;
	uint32_t newDate = RTC->DR;

	if((newDate & DATE_MASK) > 0x00170328)	// RTC clock was synchronized with DCF77 (I used my birth date in 2017 :-) )
		clockState = STATE_CHECKING_DCF77;
	else									// RTC clock was not synchronized with DCF77
		clockState = STATE_SEARCHING_DCF77;

	// Initializing tube animations
	tubeChangeDigits(NIXIE_TUBE_HH, NIXIE_DigitOff, NIXIE_DigitOff, NIXIE_AnimSwitchOn, 0);
	tubeChangeDigits(NIXIE_TUBE_HL, NIXIE_DigitOff, NIXIE_DigitOff, NIXIE_AnimSwitchOn, 0);
	tubeChangeDigits(NIXIE_TUBE_MH, NIXIE_DigitOff, NIXIE_DigitOff, NIXIE_AnimSwitchOn, 0);
	tubeChangeDigits(NIXIE_TUBE_ML, NIXIE_DigitOff, NIXIE_DigitOff, NIXIE_AnimSwitchOn, 0);

	/* Infinite loop */
	while (1)
	{
		newTime = RTC->TR;					// Get RTC time
		newDate = RTC->DR;					// Get RTC date

		switch(clockState){
			case STATE_SYNC: 			clockState = stateSync(newTime, newDate); break;
		    case STATE_CHECKING_DCF77:	clockState = stateCheckingDCF77(newTime, newDate); break;
			case STATE_SEARCHING_DCF77:	clockState = stateSearchingDCF77(newTime, newDate); break;
			case STATE_READING_DCF77:	clockState = stateReadingDCF77(newTime, newDate); break;
		}

		vl6180xIdle();
		tubeIdle();

		tubeSetAmbientLightLevel(lastLUX);

		if(lastGesture != VL6180X_GEST_NONE){
			dotsFlash(2);

			switch(lastGesture){
				case VL6180X_GEST_PUSH: tubeAmbientLightShiftUp(); break;
				case VL6180X_GEST_PULL: tubeAmbientLightShiftDown(); break;
				case VL6180X_GEST_HOLD: tubeSetAmbientLightShift(0); break;
				case VL6180X_GEST_SWIPE:
					if(tubeGetAmbientLightShift() == -9)
						tubeSetAmbientLightShift(9);
					else
						tubeAmbientLightShiftDown();

					break;
			}

			lastGesture = VL6180X_GEST_NONE;
		}
	}
}

/********************************************************************************************
 * Private functions
 ********************************************************************************************/

uint16_t stateSync(uint32_t aTime, uint32_t aDate){
	tubesAnimate(convTimeToDigits(aTime), NIXIE_AnimFade, 0x4C08);

	if(((aDate & 0x0000E000) == 0x0000E000) && ((aTime & 0x003F7F00) == 0x00040000)){
		dcfResync();			// Initialize synchronization every Sunday at 4:00am

		return STATE_CHECKING_DCF77;
	} else
		return STATE_SYNC;
}

uint16_t stateCheckingDCF77(uint32_t aTime, uint32_t aDate){
	tubesAnimate(convTimeToDigits(aTime), NIXIE_AnimFade, 0x4C08);

	if(dcfGetSyncState() == DCF_SYNC){
		dcfSetRTC();
		dotsFlash(3);

		return STATE_SYNC;
	}
		return STATE_CHECKING_DCF77;
}

uint16_t stateSearchingDCF77(uint32_t aTime, uint32_t aDate){
	static uint8_t aDigit = 0;
	static uint16_t aTube = NIXIE_TUBE_ML;

	if(dcfGetSyncState() == DCF_NOSYNC){
		if(tubeAnimInProgress() == 0){
			uint16_t tmpDigit;

			switch(aTube){
				case NIXIE_TUBE_HH: tmpDigit = (aDigit << 12) | (NIXIE_DigitOff << 8) | (NIXIE_DigitOff << 4) | NIXIE_DigitOff; break;
				case NIXIE_TUBE_HL: tmpDigit = (NIXIE_DigitOff << 12) | (aDigit << 8) | (NIXIE_DigitOff << 4) | NIXIE_DigitOff; break;
				case NIXIE_TUBE_MH: tmpDigit = (NIXIE_DigitOff << 12) | (NIXIE_DigitOff << 8) | (aDigit << 4) | NIXIE_DigitOff; break;
				case NIXIE_TUBE_ML: tmpDigit = (NIXIE_DigitOff << 12) | (NIXIE_DigitOff << 8) | (NIXIE_DigitOff << 4) | aDigit; break;
			}

			tubesAnimate(tmpDigit, NIXIE_AnimFade, 0xFFFF);

			aTube = (aTube + (uint16_t)(RNG->DR % 3) + 1) % 4;
			aDigit = (aDigit + 1) % 10;
		}

		return STATE_SEARCHING_DCF77;
	} else {
		if(tubeAnimInProgress() == 0){
			tubesOff(NIXIE_AnimFlickerOff, 0x0000);

			return STATE_READING_DCF77;
		} else
			return STATE_SEARCHING_DCF77;
	}
}

uint16_t stateReadingDCF77(uint32_t aTime, uint32_t aDate){
	switch(dcfGetSyncState()){
		case DCF_SYNC:
			dcfSetRTC();		// Set RTC time and date
			dotsFlash(3);

			if(tubeAnimInProgress() == 0){
				tubesOff(NIXIE_AnimFlickerOff, 0x4C08);

				return STATE_SYNC;
			} else
				return STATE_READING_DCF77;

		case DCF_NOSYNC:
			if(tubeAnimInProgress() == 0){
				tubesOff(NIXIE_AnimFlickerOff, 0x4C08);

				return STATE_SEARCHING_DCF77;
			} else
				return STATE_READING_DCF77;

		default:
			tubesAnimate(convDCF77ToDigits(dcfGetSyncState()), NIXIE_AnimFade, 0x4C08);

			return STATE_READING_DCF77;
	}
}

void tubesOff(uint16_t aMethod, uint16_t aDelay){
	if(tubeAnimInProgress() == 0){
		tubeChangeDigits(NIXIE_TUBE_HH, NIXIE_DigitNotChange, NIXIE_DigitOff, aMethod | NIXIE_AnimSwapDigit, (aDelay & 0xF000) >> 12);
		tubeChangeDigits(NIXIE_TUBE_HL, NIXIE_DigitNotChange, NIXIE_DigitOff, aMethod | NIXIE_AnimSwapDigit, (aDelay & 0x0F00) >> 8);
		tubeChangeDigits(NIXIE_TUBE_MH, NIXIE_DigitNotChange, NIXIE_DigitOff, aMethod | NIXIE_AnimSwapDigit, (aDelay & 0x00F0) >> 4);
		tubeChangeDigits(NIXIE_TUBE_ML, NIXIE_DigitNotChange, NIXIE_DigitOff, aMethod | NIXIE_AnimSwapDigit, (aDelay & 0x000F));
	}
}

void tubesAnimate(uint16_t aDigit, uint16_t aMethod, uint16_t aDelay){
	if(tubeAnimInProgress() == 0){
		if((aDigit & 0xF000) >> 12 != tubeGetDigit1(NIXIE_TUBE_HH))
			tubeChangeDigits(NIXIE_TUBE_HH, NIXIE_DigitNotChange, (aDigit & 0xF000) >> 12, aMethod | NIXIE_AnimSwapDigit, (aDelay & 0xF000) >> 12);

		if((aDigit & 0x0F00) >> 8 != tubeGetDigit1(NIXIE_TUBE_HL))
			tubeChangeDigits(NIXIE_TUBE_HL, NIXIE_DigitNotChange, (aDigit & 0x0F00) >> 8, aMethod | NIXIE_AnimSwapDigit, (aDelay & 0x0F00) >> 8);

		if((aDigit & 0x00F0) >> 4 != tubeGetDigit1(NIXIE_TUBE_MH))
			tubeChangeDigits(NIXIE_TUBE_MH, NIXIE_DigitNotChange, (aDigit & 0x00F0) >> 4, aMethod | NIXIE_AnimSwapDigit, (aDelay & 0x00F0) >> 4);

		if((aDigit & 0x000F) != tubeGetDigit1(NIXIE_TUBE_ML))
			tubeChangeDigits(NIXIE_TUBE_ML, NIXIE_DigitNotChange, (aDigit & 0x000F), aMethod | NIXIE_AnimSwapDigit, (aDelay & 0x000F));
	}
}

uint16_t convTimeToDigits(uint32_t aTime){
	return (uint16_t)((aTime & 0x003F7F00) >> 8);
}

uint16_t convDateToDigits(uint32_t aDate){
	return (uint16_t)(aDate & 0x00001F3F);
}

uint16_t convDCF77ToDigits(uint16_t aSyncState){
	aSyncState *= 169;

	return (((aSyncState / 1000) % 10) << 12) | (((aSyncState / 100) % 10) << 8) | (((aSyncState / 10) % 10) << 4) | (aSyncState % 10);
}

/********************************************************************************************
 * Interrupt handlers
 ********************************************************************************************/

void RTC_Alarm_IRQHandler(void) {
	if((RTC->ISR & (RTC_IT_ALRA >> 4)) != 0){
		/*
		 * The copy is performed every two RTCCLK cycles.
		 * To ensure consistency between the 3 values,
		 * reading either RTC_SSR or RTC_TR locks the values
		 * in the higher-order calendar shadow registers
		 * until RTC_DR is read.
		 */

		uint32_t aSSR = RTC->SSR;
		uint32_t aTR = RTC->TR;
		uint32_t aDR = RTC->DR;

		if((aSSR & 0x0F) == 0x0F)
			tubeAnimate(aSSR, aTR, aDR);

		dcfSync();

		EXTI->PR = EXTI_Line17;							// Clear the interrupt pending bit (EXIT)
		RTC->ISR &= ~((uint32_t)(RTC_IT_ALRA >> 4));	// Clear the interrupt pending bit (RTC)
	}
}
