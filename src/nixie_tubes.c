/*
 * NIXIETubeDriver.c
 *
 *  Created on: 03.12.2016
 *      Author: Krzysztof Malicki
 */

#include "nixie_tubes.h"

/*
 * Private definitions
 */

// Tube GPIO pins
#define NIXIE_MLTubePin		((uint16_t)0x1000)
#define NIXIE_MHTubePin		((uint16_t)0x2000)
#define NIXIE_HLTubePin		((uint16_t)0x4000)
#define NIXIE_HHTubePin		((uint16_t)0x8000)

#define NIXIE_TubePinMask	((uint16_t)0xF000)

// Digit on the tube
#define NIXIE_DigitNone		((uint16_t)0x0000)
#define NIXIE_Digit0		((uint16_t)0x0001)
#define NIXIE_Digit1		((uint16_t)0x0002)
#define NIXIE_Digit2		((uint16_t)0x0004)
#define NIXIE_Digit3		((uint16_t)0x0008)
#define NIXIE_Digit4		((uint16_t)0x0010)
#define NIXIE_Digit5		((uint16_t)0x0020)
#define NIXIE_Digit6		((uint16_t)0x0040)
#define NIXIE_Digit7		((uint16_t)0x0080)
#define NIXIE_Digit8		((uint16_t)0x0100)
#define NIXIE_Digit9		((uint16_t)0x0200)
#define NIXIE_DigitMask		((uint16_t)0x03FF)

// Random flicker table length
#define NIXIE_FLTAB_LEN 20

/*
 * Private classes
 */

typedef struct
{
	uint16_t animState;		// The animation type and state register
	uint16_t animDelay;		// The animation delay register (general purpose)
	uint32_t animProgress;	// The animation progress (general purpose)
} NIXIE_AnimStruct;

/*
 * Private values
 */
static uint16_t curTube = NIXIE_TUBE_ML;
							// The tube that is switched on
static uint16_t nxtTube = NIXIE_TUBE_MH;
							// The next tube to be switched on
static uint16_t curDigits1[4] = {NIXIE_DigitNone, NIXIE_DigitNone, NIXIE_DigitNone, NIXIE_DigitNone};
							// Current main digits on the tubes
static uint16_t curDigits2[4] = {NIXIE_DigitNone, NIXIE_DigitNone, NIXIE_DigitNone, NIXIE_DigitNone};
							// Current supplementary digits on the tubes
static uint16_t curPulse[4] = {0, 0, 0, 0};
							// Current pulse lengths on the tubes
static uint16_t pwmPulseChangedFlag = 0;
							// Flag set when pwm pulse of a tube is changing (should be set to the number of tubes)
static NIXIE_AnimStruct curAnims[4] = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
							// Current animations
static uint16_t numFlashes = 0;
							// Remaining dot flashes before resuming normal operation
static uint16_t newARR = NIXIE_TIM_PERIOD;
							// Target max brightness level
static int8_t shiftARR = 0;	// Shift in brightness level [-9 .. 9]. Higher shift means lower brightness

/*
 * Public methods
 */

void dotsFlash(uint16_t nTimes){
	numFlashes = nTimes * DOT_FLASH_DURATION;
}

void tubeChangeDigits(uint16_t aTube, uint16_t aDigit1, uint16_t aDigit2, uint16_t aMethod, uint16_t aDelay)
{
	const uint32_t flkTab[NIXIE_FLTAB_LEN] =		// Random flicker table
	{
		1600324737, 1563891781, 1341475029, 1119482113,
		1590126849, 1603538609, 1590507553, 1595905025,
		1475268225, 1542789641, 1531310657, 1597165857,
		1568310835, 1507462305, 1539792973, 1572656137,
		1591594033, 1537132161, 1609019809, 1583259241
	};

	static uint16_t curFklTabItem = 0;				// Current position in the flicker table

	if(aTube > NIXIE_TUBE_HH) return;

	tubeSetDigits(aTube, aDigit1, aDigit2);

	aMethod |= (uint16_t)NIXIE_AnimInProgress;

	curAnims[aTube].animState = 0;
	curAnims[aTube].animDelay = aDelay;
	curAnims[aTube].animProgress = 0;

	switch(aMethod & NIXIE_AnimMethodMask){
		case NIXIE_AnimSwitchOff:
			curPulse[aTube] = 0;

			pwmPulseChangedFlag = 4;

			break;

		case NIXIE_AnimSwitchOn:
			curPulse[aTube] = NIXIE_MAX_BRIGHTNESS;

			pwmPulseChangedFlag = 4;

			break;

		case NIXIE_AnimFlickerOff:
			curAnims[aTube].animProgress = flkTab[curFklTabItem++];
			curFklTabItem %= NIXIE_FLTAB_LEN;

		case NIXIE_AnimFade:
			if(curPulse[aTube] > 0)
				curAnims[aTube].animState = aMethod;

			break;

		case NIXIE_AnimFlickerOn:
			curAnims[aTube].animProgress = flkTab[curFklTabItem++];
			curFklTabItem %= NIXIE_FLTAB_LEN;

		case NIXIE_AnimAppear:
			if(curPulse[aTube] < NIXIE_MAX_BRIGHTNESS)
				curAnims[aTube].animState = aMethod;

			break;

		case NIXIE_AnimBlink:
		case NIXIE_AnimFlash:
			curAnims[aTube].animState = aMethod;

		break;
	}
}

void tubeSetDigits(uint16_t aTube, uint16_t aDigit1, uint16_t aDigit2){
	if(aTube > NIXIE_TUBE_HH) return;

	if(aDigit1 != NIXIE_DigitNotChange){
		if(aDigit1 > 9)
			curDigits1[aTube] = 0x0000;
		else
			curDigits1[aTube] = 0x0001 << aDigit1;
	}

	if(aDigit2 != NIXIE_DigitNotChange){
		if(aDigit2 > 9)
			curDigits2[aTube] = 0x0000;
		else
			curDigits2[aTube] = 0x0001 << aDigit2;
	}
}

void tubeSetBrightness(uint16_t aTube, uint16_t newBrightness){
	if(aTube <= NIXIE_TUBE_HH){
		if(newBrightness > NIXIE_MAX_BRIGHTNESS)
			curPulse[aTube] = NIXIE_MAX_BRIGHTNESS;
		else
			curPulse[aTube] = newBrightness;

		pwmPulseChangedFlag = 4;
	}
}

void tubeSetAmbientLightLevel(uint32_t newLux){
	const uint32_t shiftTab[19] =	// ARR shift table
	{
		   26,    39,    59,    88,   132,   198,   296,   444,   667,	// shiftARR = -9 to -1
		 1000,															// shiftARR = 0
		 1500,  2250,  3375,  5063,  7594, 11390, 17086, 25629, 38443	// shiftARR = 1 to 9
	};

	uint32_t tmpARR = LUX_MAX_BRIGHTNESS * NIXIE_TIM_PERIOD;

	if(newLux > 0)
		tmpARR = (LUX_MAX_BRIGHTNESS * NIXIE_TIM_PERIOD) / newLux;

	if(((tmpARR >= 0xFFFF) && (shiftARR > 0)) ||
	   ((tmpARR <= NIXIE_TIM_PERIOD) && (shiftARR < 0))){
		// Do not shift because already out of range
	} else
		if(shiftARR != 0) tmpARR = (tmpARR * shiftTab[shiftARR + 9]) / 1000;

	if(tmpARR < NIXIE_TIM_PERIOD)
		tmpARR = NIXIE_TIM_PERIOD;					// Initial (minimum) TIM period
	else
		if(tmpARR > 0xFFFF)
			tmpARR = 0xFFFF;						// Maximum 16bit TIM period

	newARR = (uint16_t)tmpARR;
}

void tubeSetAmbientLightShift(int8_t newShift){
	if(newShift > 9)
		shiftARR = 9;
	else
		if(newShift < -9)
			shiftARR = -9;
		else
			shiftARR = newShift;
}

void tubeAmbientLightShiftUp(){
	if(shiftARR < 9) shiftARR++;
}

void tubeAmbientLightShiftDown(){
	if(shiftARR > -9) shiftARR--;
}

void tubeStopAnim(uint16_t aTube, uint16_t aFlag){
	if(aTube <= NIXIE_TUBE_HH)
		curAnims[aTube].animState |= (uint16_t)(NIXIE_AnimStopReq | (aFlag & NIXIE_AnimStopOn));
}

void tubeWaitForAnim(){
	while(tubeAnimInProgress());
}

uint16_t tubeAnimInProgress(){
	if((((curAnims[NIXIE_TUBE_ML].animState |
		  curAnims[NIXIE_TUBE_MH].animState |
		  curAnims[NIXIE_TUBE_HL].animState |
		  curAnims[NIXIE_TUBE_HH].animState) & NIXIE_AnimInProgress) != 0) || (pwmPulseChangedFlag != 0))
		return 1;
	else
		return 0;
}

uint16_t tubeGetDigit1(uint16_t aTube){
	if(aTube > NIXIE_TUBE_HH) return NIXIE_DigitOff;

	switch(curDigits1[aTube]){
		case NIXIE_Digit0:	return 0;
		case NIXIE_Digit1:	return 1;
		case NIXIE_Digit2:	return 2;
		case NIXIE_Digit3:	return 3;
		case NIXIE_Digit4:	return 4;
		case NIXIE_Digit5:	return 5;
		case NIXIE_Digit6:	return 6;
		case NIXIE_Digit7:	return 7;
		case NIXIE_Digit8:	return 8;
		case NIXIE_Digit9:	return 9;
		default: 			return NIXIE_DigitOff;
	}
}

uint16_t tubeGetDigit2(uint16_t aTube){
	if(aTube > NIXIE_TUBE_HH) return NIXIE_DigitOff;

	switch(curDigits2[aTube]){
		case NIXIE_Digit0:	return 0;
		case NIXIE_Digit1:	return 1;
		case NIXIE_Digit2:	return 2;
		case NIXIE_Digit3:	return 3;
		case NIXIE_Digit4:	return 4;
		case NIXIE_Digit5:	return 5;
		case NIXIE_Digit6:	return 6;
		case NIXIE_Digit7:	return 7;
		case NIXIE_Digit8:	return 8;
		case NIXIE_Digit9:	return 9;
		default: 			return NIXIE_DigitOff;
	}
}

uint16_t tubeGetBrightness(uint16_t aTube){
	if(aTube <= NIXIE_TUBE_HH)
		return curPulse[aTube];
	else
		return 0;
}

int8_t tubeGetAmbientLightShift(){
	return shiftARR;
}

void tubeAnimate(uint32_t aSSR, uint32_t aTR, uint32_t aDR){
	// Animate dots
	if(numFlashes > 0){
		if((--numFlashes & (DOT_FLASH_DURATION >> 1)) == 0){
			TIM3->CCR1 = 0;
			TIM3->CCR2 = 0;
		} else {
			TIM3->CCR1 = NIXIE_MAX_BRIGHTNESS;
			TIM3->CCR2 = NIXIE_MAX_BRIGHTNESS;
		}
	} else {
		if(TIM3->CCR1 > 24){
			TIM3->CCR1 -= 24;
			TIM3->CCR2 -= 24;
		} else {
			TIM3->CCR1 = 0;
			TIM3->CCR2 = 0;

			if(((aTR & 0x0000000F) % 5) == 0x00000000){
				TIM3->CCR1 = NIXIE_MAX_BRIGHTNESS;
				TIM3->CCR2 = NIXIE_MAX_BRIGHTNESS;
			}
		}
	}

	// Animate tubes
	for(uint16_t i = 0; i <= NIXIE_TUBE_HH; i++){
		if(curAnims[i].animState & NIXIE_AnimInProgress){
			if(curAnims[i].animDelay == 0){
				switch(curAnims[i].animState & NIXIE_AnimMethodMask){
					case NIXIE_AnimFade:
						if(curPulse[i] <= 32){
							if(curAnims[i].animState & NIXIE_AnimSwapDigit){
								curPulse[i]   = NIXIE_MAX_BRIGHTNESS;
								curDigits1[i] = curDigits2[i];
								curDigits2[i] = NIXIE_DigitNone;
							} else
								curPulse[i] = 0;

							curAnims[i].animState = 0;	// Terminate animation
						} else
							curPulse[i] -= 32;

						pwmPulseChangedFlag = 4;

						break;

					case NIXIE_AnimAppear:
						if(curPulse[i] >= NIXIE_MAX_BRIGHTNESS - 32){
							if(curAnims[i].animState & NIXIE_AnimSwapDigit){
								curPulse[i]   = 0;
								curDigits2[i] = curDigits1[i];
								curDigits1[i] = NIXIE_DigitNone;
							} else
								curPulse[i] = NIXIE_MAX_BRIGHTNESS;

							curAnims[i].animState = 0;	// Terminate animation
						} else
							curPulse[i] += 32;

						pwmPulseChangedFlag = 4;

						break;

					case NIXIE_AnimFlickerOff:
						if(curAnims[i].animProgress & 0x00000001)
							curPulse[i] = 0;					// Off
						else
							curPulse[i] = NIXIE_MAX_BRIGHTNESS;	// On

						curAnims[i].animProgress >>= 1;

						if(curAnims[i].animProgress == 0){
							if(curAnims[i].animState & NIXIE_AnimSwapDigit){
								curPulse[i]   = NIXIE_MAX_BRIGHTNESS;
								curDigits1[i] = curDigits2[i];
								curDigits2[i] = NIXIE_DigitNone;
							}

							curAnims[i].animState = 0;	// Terminate animation
						}

						pwmPulseChangedFlag = 4;

						break;

					case NIXIE_AnimFlickerOn:
						if(curAnims[i].animProgress & 0x00000001)
							curPulse[i] = NIXIE_MAX_BRIGHTNESS;	// On
						else
							curPulse[i] = 0;					// Off

						curAnims[i].animProgress >>= 1;

						if(curAnims[i].animProgress == 0){
							if(curAnims[i].animState & NIXIE_AnimSwapDigit){
								curPulse[i]   = 0;
								curDigits2[i] = curDigits1[i];
								curDigits1[i] = NIXIE_DigitNone;
							}

							curAnims[i].animState = 0;	// Terminate animation
						}

						pwmPulseChangedFlag = 4;

						break;

					case NIXIE_AnimBlink:
						if(curAnims[i].animProgress++ == 15){
							curAnims[i].animProgress = 0;

							if(curPulse[i] == 0){
								curPulse[i] = NIXIE_MAX_BRIGHTNESS;

								if((curAnims[i].animState & (NIXIE_AnimStopReq | NIXIE_AnimStopOn)) == (NIXIE_AnimStopReq | NIXIE_AnimStopOn))
									curAnims[i].animState = 0;	// Terminate animation
							} else {
								curPulse[i] = 0;

								if((curAnims[i].animState & (NIXIE_AnimStopReq | NIXIE_AnimStopOn)) == NIXIE_AnimStopReq)
									curAnims[i].animState = 0;	// Terminate animation
							}

							pwmPulseChangedFlag = 4;
						}

						break;

					case NIXIE_AnimFlash:
						if(curPulse[i] > NIXIE_MAX_BRIGHTNESS - 64)
							if(curAnims[i].animState & NIXIE_AnimStopReq){
								if(curAnims[i].animState & NIXIE_AnimStopOn)
									curPulse[i] = NIXIE_MAX_BRIGHTNESS;
								else
									curPulse[i] = 0;

								curAnims[i].animState = 0;	// Terminate animation
							} else
								curPulse[i] = 0;
						else
							curPulse[i] += 64;

						pwmPulseChangedFlag = 4;

						break;
				}
			} else
				curAnims[i].animDelay--;
		}
	}
}

void tubeIdle(){
	if(TIM3->ARR != newARR){
		if(TIM3->ARR == NIXIE_TIM_PERIOD)
			TIM3->DIER |= TIM_IT_CC4;

		if(newARR == NIXIE_TIM_PERIOD)
			TIM3->DIER &= (uint16_t)~TIM_IT_CC4;

		TIM3->ARR = newARR;
	}
}

/*
 * Private methods
 */

void TIM3_IRQHandler(void)
{
	uint16_t tmpSR = TIM3->SR & TIM3->DIER;

	if(tmpSR & TIM_IT_Update){					// Starting service of the next tube
		switch(TIM3->CCR3){
			case NIXIE_MAX_BRIGHTNESS:			// Only main digit (digit1)
				TIM3->DIER &= (uint16_t)~TIM_IT_CC3;
												// Disable the interrupt

				GPIOD->ODR = (NIXIE_MLTubePin << nxtTube) | curDigits1[nxtTube];

				break;
			case 0:								// Only supplementary digit (digit2)
				TIM3->DIER &= (uint16_t)~TIM_IT_CC3;
												// Disable the interrupt
				GPIOD->ODR = (NIXIE_MLTubePin << nxtTube) | curDigits2[nxtTube];

				break;
			default:							// both digits
				TIM3->DIER |= TIM_IT_CC3;		// Enable the interrupt

				GPIOD->ODR = (NIXIE_MLTubePin << nxtTube) | curDigits1[nxtTube];
		}

		curTube = nxtTube++;
		nxtTube %= 4;

		if(pwmPulseChangedFlag != 0)
			pwmPulseChangedFlag--;				// Wait a full round of tube service until the flag is reset

		TIM3->CCR3 = curPulse[nxtTube];			// Setting pulse length for the next tube

		TIM3->SR &= (uint16_t)~(TIM_IT_Update | TIM_IT_CC3);
												// Clear Interrupt pending bit
	}

	if(tmpSR & TIM_IT_CC3){						// Time to change from digit1 to digit2
		GPIOD->ODR = (NIXIE_MLTubePin << curTube) | curDigits2[curTube];
												// Switch from digit1 to digit2

		TIM3->SR &= (uint16_t)~TIM_IT_CC3;		// Clear Interrupt pending bit
	}

	if(tmpSR & TIM_IT_CC4){						// Time to switch off the current tube
		GPIOD->ODR = NIXIE_DigitNone;

		TIM3->SR &= (uint16_t)~TIM_IT_CC4;		// Clear Interrupt pending bit
	}
}
