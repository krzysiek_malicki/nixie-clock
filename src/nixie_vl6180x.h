/*
 * nixie_vl6180x.h
 *
 *  Created on: 16.06.2017
 *      Author: Krzysztof Malicki
 */

#ifndef NIXIE_VL6180X_H_
#define NIXIE_VL6180X_H_

#include "stm32f4xx.h"
#include "nixie_conf.h"

/*
 * Public definitions
 */

/********************************************************************************************
 * vl6180x gestures
 ********************************************************************************************/
#define VL6180X_GEST_NONE			0						// No gesture detected
#define VL6180X_GEST_SWIPE			1						// Hand swiped over the sensor
#define VL6180X_GEST_PUSH			2						// Hand pushed down over the sensor
#define VL6180X_GEST_PULL			3						// Hand pulled up over the sensor
#define VL6180X_GEST_HOLD			4						// Hand held over the sensor

/********************************************************************************************
 * I2C statuses
 ********************************************************************************************/
#define I2C_OK						0		// No timeout (operation successful)
#define I2C_ERROR_START				1		// Timeout when generating start condition
#define I2C_ERROR_ADDRESS			2		// Timeout when transmitting device I2C address
#define I2C_ERROR_RECEIVE			3		// Timeout when receiving data
#define I2C_ERROR_TRANSMIT			4		// Timeout when transmitting data

/********************************************************************************************
 * vl6180x exported variables
 ********************************************************************************************/
extern uint16_t lastGesture;								// Last recognized gesture
extern uint32_t lastLUX;									// Last measured ambient light intensity

/*
 * Public methods prototypes
 */
void vl6180xInitDevice();
void vl6180xIdle();

#endif /* NIXIE_VL6180X_H_ */
