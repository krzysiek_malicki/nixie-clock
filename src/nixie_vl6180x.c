/*
 * nixie_vl6180x.c
 *
 *  Created on: 16.06.2017
 *      Author: Krzysztof Malicki
 */

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_it.h"
#include "vl6180x_api.h"
#include "nixie_vl6180x.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define VL6180X_STATE_IDLE	0	// No hand over the sensor
#define VL6180X_STATE_HAND	1	// Hand over the sensor detected
#define VL6180X_STATE_PUSH	2	// Hand pushed down over the sensor
#define VL6180X_STATE_PULL	3	// Hand pulled up over the sensor

/* Private macro -------------------------------------------------------------*/
#define VL6180x_AlsGetStatus(dev, status) VL6180x_RdByte(dev, RESULT_ALS_STATUS, status)
#define VL6180x_RangeGetStatus(dev, status) VL6180x_RdByte(dev, RESULT_RANGE_STATUS, status)
#define VL6180x_AlsStartSingleShot(dev) VL6180x_AlsSetSystemMode(dev, MODE_SINGLESHOT | MODE_START_STOP)

/* Private variables ---------------------------------------------------------*/
static uint16_t actState = VL6180X_STATE_IDLE;					// The actual gesture recognition state
static uint16_t prevState = VL6180X_STATE_IDLE;					// The previous gesture recognition state
static uint32_t	cntLastEvent = 0;								// Time stamp of the last detected gesture
static uint32_t cntI2CAccess = 0;								// I2C access counter (for DEBUG only)

/* Private function prototypes -----------------------------------------------*/
void SetupVL6180XInterrupt(uint16_t aState);

/* External variables --------------------------------------------------------*/
extern uint16_t cntVL6180xMeasurementTick;						// VL6180x measurement interval counter (1 ms tick)
extern uint16_t cntVL6180xGestureTick;							// VL6180x gesture recognition counter (1 ms tick)

/* Exported variables --------------------------------------------------------*/
uint16_t lastGesture = VL6180X_GEST_NONE;						// Last recognized gesture
uint32_t lastLUX = 0;											// Last measured ambient light intensity

/* Exported functions --------------------------------------------------------*/
void vl6180xInitDevice(){
	// Basic initialization of the device
	VL6180x_WaitDeviceBooted(VL6180X_I2C_ADDR);
	VL6180x_InitData(VL6180X_I2C_ADDR);

	VL6180x_Prepare(VL6180X_I2C_ADDR);

	VL6180x_DMaxSetState(VL6180X_I2C_ADDR, 0);
	VL6180x_FilterSetState(VL6180X_I2C_ADDR, 0);
	VL6180x_RangeSetEceState(VL6180X_I2C_ADDR, 1);
	VL6180x_RangeSetMaxConvergenceTime(VL6180X_I2C_ADDR, VL6180X_RANGE_CONVERGENCE);

	VL6180x_AlsSetAnalogueGain(VL6180X_I2C_ADDR, VL6180X_ALS_GAIN);
	VL6180x_AlsSetIntegrationPeriod(VL6180X_I2C_ADDR, VL6180X_ALS_INTEGRATION);

	VL6180x_SetupGPIO1(VL6180X_I2C_ADDR, GPIOx_SELECT_GPIO_INTERRUPT_OUTPUT, INTR_POL_HIGH);

	VL6180x_ClearAllInterrupt(VL6180X_I2C_ADDR);
	VL6180x_AlsConfigInterrupt(VL6180X_I2C_ADDR, CONFIG_GPIO_INTERRUPT_NEW_SAMPLE_READY);
	VL6180x_RangeConfigInterrupt(VL6180X_I2C_ADDR, CONFIG_GPIO_INTERRUPT_LEVEL_LOW);

	VL6180x_RangeSetThresholds(VL6180X_I2C_ADDR, VL6180X_SWIPE_DIST, VL6180X_SWIPE_DIST, 0);

/*
 * According to the official information from the ST support site,
 * continuous mode is not working properly together with ALS asynchronous single-shot mode.
 * I had to reprogram to single-shot measurements for both Range and ALS.
 *
	VL6180x_RangeSetInterMeasPeriod(VL6180X_I2C_ADDR, VL6180X_MEAS_INTERVAL);
	VL6180x_RangeStartContinuousMode(VL6180X_I2C_ADDR);
*/
}

void vl6180xIdle(){
	int32_t actRange;
	uint8_t itStatus;
	// uint8_t devStatus;

	// Time out for gesture recognition --> reset to idle state
	if((actState != VL6180X_STATE_IDLE) && (cntVL6180xGestureTick >= VL6180X_RESET_TIME)){
		SetupVL6180XInterrupt(VL6180X_STATE_IDLE);

		VL6180x_RangeClearInterrupt(VL6180X_I2C_ADDR);
	}

	// Check the measurement status (via GPIO1 interrupt)
	if((VL6180X_GPIO->IDR & VL6180X_GPIO_PIN) != 0){
		// Measurement ready --> perform gesture detection procedure

		VL6180x_GetInterruptStatus(VL6180X_I2C_ADDR, &itStatus);

		cntI2CAccess++;

		if((itStatus & CONFIG_GPIO_ALS_MASK) != 0){
			VL6180x_AlsGetLux(VL6180X_I2C_ADDR, &lastLUX);
//			VL6180x_AlsGetStatus(VL6180X_I2C_ADDR, &devStatus);
			VL6180x_AlsClearInterrupt(VL6180X_I2C_ADDR);
		}

		if((itStatus & CONFIG_GPIO_RANGE_MASK) != 0){
			VL6180x_RangeGetResult(VL6180X_I2C_ADDR, &actRange);
//			VL6180x_RangeGetStatus(VL6180X_I2C_ADDR, &devStatus);
			VL6180x_RangeClearInterrupt(VL6180X_I2C_ADDR);

			switch(actState){
				case VL6180X_STATE_IDLE:						// No hand over the sensor (no gesture detected in progress)
					if(actRange < VL6180X_PUSH_DIST)
						SetupVL6180XInterrupt(VL6180X_STATE_PUSH);
					else
						SetupVL6180XInterrupt(VL6180X_STATE_HAND);

					break;

				case VL6180X_STATE_HAND:						// Hand over the sensor detected
					if(actRange > VL6180X_MAX_DIST){
																// No hand over the sensor (or hand above the max distance)
						if((prevState == VL6180X_STATE_IDLE) && (cntVL6180xGestureTick < VL6180X_SWIPE_TIME))
							lastGesture = VL6180X_GEST_SWIPE;	// --> SWIPE detected

						SetupVL6180XInterrupt(VL6180X_STATE_IDLE);
					} else
						if(actRange >= VL6180X_PULL_DIST)
							SetupVL6180XInterrupt(VL6180X_STATE_PULL);
																// Hand moved up --> wait for PULL gesture
						else
							SetupVL6180XInterrupt(VL6180X_STATE_PUSH);
																// Hand moved down --> wait for PUSH gesture

					break;

				case VL6180X_STATE_PUSH:						// Hand pushed down over the sensor
					if(actRange > VL6180X_MAX_DIST){
																// No hand over the sensor (or hand above the max distance)
						if((prevState == VL6180X_STATE_IDLE) && (cntVL6180xGestureTick < VL6180X_SWIPE_TIME))
							lastGesture = VL6180X_GEST_SWIPE;	// --> SWIPE detected

						SetupVL6180XInterrupt(VL6180X_STATE_IDLE);
					} else {
						if((prevState != VL6180X_STATE_IDLE) && (cntVL6180xGestureTick < VL6180X_HOLD_TIME))
							lastGesture = VL6180X_GEST_PUSH;	// --> PUSH detected

						SetupVL6180XInterrupt(VL6180X_STATE_HAND);
					}

					break;

				case VL6180X_STATE_PULL:						// Hand pulled up over the sensor
					if(actRange > VL6180X_MAX_DIST)
																// No hand over the sensor (or hand above the max distance)
						SetupVL6180XInterrupt(VL6180X_STATE_IDLE);
					else {
						if(cntVL6180xGestureTick < VL6180X_HOLD_TIME)
							lastGesture = VL6180X_GEST_PULL;	// --> PULL detected

						SetupVL6180XInterrupt(VL6180X_STATE_HAND);
					}

					break;
			}
		}
	}

	// Check timeouts and auto events
	switch(actState){
		case VL6180X_STATE_IDLE:								// No hand over the sensor (no gesture detected in progress)
			// TODO: You may reset the sensor occasionally

			break;

		case VL6180X_STATE_HAND:								// Hand over the sensor detected
			if(cntVL6180xGestureTick - cntLastEvent >= VL6180X_HOLD_TIME){
				lastGesture = VL6180X_GEST_HOLD;				// --> HOLD detected

				cntLastEvent += VL6180X_AUTO_TIME;
			}

			break;

		case VL6180X_STATE_PUSH:								// Hand pushed down over the sensor
			if((prevState != VL6180X_STATE_IDLE) && (cntVL6180xGestureTick - cntLastEvent > VL6180X_HOLD_TIME)){
				lastGesture = VL6180X_GEST_PUSH;				// --> PUSH detected

				cntLastEvent += VL6180X_AUTO_TIME;
			}

			break;

		case VL6180X_STATE_PULL:								// Hand pulled up over the sensor
			if(cntVL6180xGestureTick - cntLastEvent >= VL6180X_HOLD_TIME){
				lastGesture = VL6180X_GEST_PULL;				// --> PULL detected

				cntLastEvent += VL6180X_AUTO_TIME;
			}

			break;
	}

	// Start the measurement
	if((actState != VL6180X_STATE_IDLE) && (cntVL6180xMeasurementTick >= VL6180X_RCNG_INTERVAL)){
		VL6180x_RangeStartSingleShot(VL6180X_I2C_ADDR);				// Start Range single shot mode

		cntVL6180xMeasurementTick = 0;
	} else
		if(cntVL6180xMeasurementTick >= VL6180X_IDLE_INTERVAL){
			if(cntVL6180xGestureTick > VL6180X_ALS_TIME){
				VL6180x_AlsStartSingleShot(VL6180X_I2C_ADDR);		// Start ALS single shot mode

				cntVL6180xGestureTick = 0;
			} else {
				VL6180x_RangeStartSingleShot(VL6180X_I2C_ADDR);		// Start Range single shot mode
			}

			cntVL6180xMeasurementTick = 0;
		}
}

/* Private functions ---------------------------------------------------------*/

/********************************************************************************************
 * Continuous measurement interrupt configuration
 ********************************************************************************************/
void SetupVL6180XInterrupt(uint16_t aState){
	VL6180x_SetGroupParamHold(VL6180X_I2C_ADDR, 1);

	switch(aState){
		case VL6180X_STATE_IDLE:	// No hand over the sensor
			VL6180x_RangeConfigInterrupt(VL6180X_I2C_ADDR, CONFIG_GPIO_INTERRUPT_LEVEL_LOW);
			VL6180x_RangeSetThresholds(VL6180X_I2C_ADDR, VL6180X_SWIPE_DIST, VL6180X_SWIPE_DIST, 0);

			break;

		case VL6180X_STATE_HAND:	// Hand over the sensor detected
			VL6180x_RangeConfigInterrupt(VL6180X_I2C_ADDR, CONFIG_GPIO_INTERRUPT_OUT_OF_WINDOW);
			VL6180x_RangeSetThresholds(VL6180X_I2C_ADDR, VL6180X_PUSH_DIST, VL6180X_PULL_DIST, 0);

			break;

		case VL6180X_STATE_PUSH:	// Hand pushed down over the sensor
			VL6180x_RangeConfigInterrupt(VL6180X_I2C_ADDR, CONFIG_GPIO_INTERRUPT_LEVEL_HIGH);
			VL6180x_RangeSetThresholds(VL6180X_I2C_ADDR, VL6180X_PUSH_DIST, VL6180X_PUSH_DIST, 0);

			break;

		case VL6180X_STATE_PULL:	// Hand pulled up over the sensor
			VL6180x_RangeConfigInterrupt(VL6180X_I2C_ADDR, CONFIG_GPIO_INTERRUPT_OUT_OF_WINDOW);
			VL6180x_RangeSetThresholds(VL6180X_I2C_ADDR, VL6180X_PULL_DIST, VL6180X_MAX_DIST, 0);

			break;
	}

	VL6180x_SetGroupParamHold(VL6180X_I2C_ADDR, 0);

	cntVL6180xGestureTick = 0;		// Reset timer
	cntLastEvent = 0;				// Reset time stamp

	prevState = actState;
	actState = aState;				// Set new gesture detection state state
}

/********************************************************************************************
 * I2C transmission for vl6180x device
 ********************************************************************************************/
int VL6180x_I2CRead(VL6180xDev_t dev, uint8_t *buff, uint8_t len){
	int tmpResult = I2C_OK;

	uint32_t tmpSysTick = cntSysTick;
	uint16_t tmpTimeout = I2C_TIMEOUT;

	I2C_GenerateSTART(I2C1, ENABLE);
	while ((I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT) != SUCCESS) && (tmpResult == I2C_OK)){
		if(tmpSysTick != cntSysTick){
			if(tmpTimeout-- > 0)
				tmpSysTick = cntSysTick;
			else
				tmpResult = I2C_ERROR_START;
		}
	}

	tmpTimeout = I2C_TIMEOUT;

	I2C_Send7bitAddress(I2C1, dev, I2C_Direction_Receiver);
	while ((I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED) != SUCCESS) && (tmpResult == I2C_OK)){
		if(tmpSysTick != cntSysTick){
			if(tmpTimeout-- > 0)
				tmpSysTick = cntSysTick;
			else
				tmpResult = I2C_ERROR_ADDRESS;
		}
	}

	if(len > 1)
		I2C_AcknowledgeConfig(I2C1, ENABLE);
	else {
		I2C_AcknowledgeConfig(I2C1, DISABLE);
		I2C_GenerateSTOP(I2C1, ENABLE);
	}

	while(len-- > 0){
		tmpTimeout = I2C_TIMEOUT;

		while((I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_RECEIVED) != SUCCESS) && (tmpResult == I2C_OK)){
			if(tmpSysTick != cntSysTick){
				if(tmpTimeout-- > 0)
					tmpSysTick = cntSysTick;
				else
					tmpResult = I2C_ERROR_RECEIVE;
			}
		}

		*(buff++) = I2C_ReceiveData(I2C1);

		if(len == 1){
			I2C_AcknowledgeConfig(I2C1, DISABLE);
			I2C_GenerateSTOP(I2C1, ENABLE);
		}
	}

	return tmpResult;
}

int VL6180x_I2CWrite(VL6180xDev_t dev, uint8_t  *buff, uint8_t len){
	int tmpResult = I2C_OK;

	uint32_t tmpSysTick = cntSysTick;
	uint16_t tmpTimeout = I2C_TIMEOUT;

	I2C_GenerateSTART(I2C1, ENABLE);
	while ((I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT) != SUCCESS) && (tmpResult == I2C_OK)){
		if(tmpSysTick != cntSysTick){
			if(tmpTimeout-- > 0)
				tmpSysTick = cntSysTick;
			else
				tmpResult = I2C_ERROR_START;
		}
	}

	tmpTimeout = I2C_TIMEOUT;

	I2C_Send7bitAddress(I2C1, dev, I2C_Direction_Transmitter);
	while ((I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED) != SUCCESS) && (tmpResult == I2C_OK)){
		if(tmpSysTick != cntSysTick){
			if(tmpTimeout-- > 0)
				tmpSysTick = cntSysTick;
			else
				tmpResult = I2C_ERROR_ADDRESS;
		}
	}

	while(len-- > 0){
		tmpTimeout = I2C_TIMEOUT;

		while ((I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTING) != SUCCESS) && (tmpResult == I2C_OK)){
			if(tmpSysTick != cntSysTick){
				if(tmpTimeout-- > 0)
					tmpSysTick = cntSysTick;
				else
					tmpResult = I2C_ERROR_TRANSMIT;
			}
		}

		I2C_SendData(I2C1, *(buff++));
	}

	tmpTimeout = I2C_TIMEOUT;

	while ((I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTED) != SUCCESS) && (tmpResult == I2C_OK)){
		if(tmpSysTick != cntSysTick){
			if(tmpTimeout-- > 0)
				tmpSysTick = cntSysTick;
			else
				tmpResult = I2C_ERROR_TRANSMIT;
		}
	}

	I2C_GenerateSTOP(I2C1, ENABLE);

	return tmpResult;
}
