/*
 * NIXIETubeDriver.h
 *
 *  Created on: 03.12.2016
 *      Author: Krzysztof Malicki
 */

#ifndef NIXIE_TUBES_H_
#define NIXIE_TUBES_H_

#include "stm32f4xx.h"
#include "nixie_conf.h"

/*
 * Public definitions
 */

// Tubes
#define	NIXIE_TUBE_ML			((uint16_t)0x0000)
#define NIXIE_TUBE_MH			((uint16_t)0x0001)
#define NIXIE_TUBE_HL			((uint16_t)0x0002)
#define NIXIE_TUBE_HH			((uint16_t)0x0003)
#define NIXIE_TUBE_NONE			((uint16_t)0x000F)

// All digits off
#define NIXIE_DigitOff			((uint16_t)0x000F)	// Used in NTDSetDigits & NTDGetDigit1 & NTDGetDigit2
#define NIXIE_DigitNotChange	((uint16_t)0x000E)

// Animation methods
#define NIXIE_AnimNone			((uint16_t)0x0000)
#define NIXIE_AnimSwitchOff		((uint16_t)0x1000)
#define NIXIE_AnimSwitchOn		((uint16_t)0x2000)
#define NIXIE_AnimFade			((uint16_t)0x3000)
#define NIXIE_AnimAppear		((uint16_t)0x4000)
#define NIXIE_AnimFlickerOff	((uint16_t)0x5000)
#define NIXIE_AnimFlickerOn		((uint16_t)0x6000)
#define NIXIE_AnimBlink			((uint16_t)0x7000)
#define NIXIE_AnimFlash			((uint16_t)0x8000)

#define NIXIE_AnimMethodMask	((uint16_t)0xF000)

// Animation flags
#define NIXIE_AnimInProgress	((uint16_t)0x0100)

#define NIXIE_AnimSwapDigit		((uint16_t)0x0010)
#define NIXIE_AnimStopReq		((uint16_t)0x0040)
#define NIXIE_AnimStopOn		((uint16_t)0x0080)

/*
 * Public methods prototypes
 */
void dotsFlash(uint16_t nTimes);
void tubeAnimate(uint32_t aSSR, uint32_t aTR, uint32_t aDR);
void tubeIdle();
void tubeChangeDigits(uint16_t aTube, uint16_t aDigit1, uint16_t aDigit2, uint16_t aMethod, uint16_t aDelay);
void tubeSetDigits(uint16_t aTube, uint16_t aDigit1, uint16_t aDigit2);
void tubeSetBrightness(uint16_t aTube, uint16_t newBrightness);
void tubeSetAmbientLightLevel(uint32_t newLux);
void tubeSetAmbientLightShift(int8_t newShift);
void tubeAmbientLightShiftUp();
void tubeAmbientLightShiftDown();
void tubeStopAnim(uint16_t aTube, uint16_t aFlag);
void tubeWaitForAnim();
uint16_t tubeAnimInProgress();
uint16_t tubeGetDigit1(uint16_t aTube);
uint16_t tubeGetDigit2(uint16_t aTube);
uint16_t tubeGetBrightness(uint16_t aTube);
int8_t tubeGetAmbientLightShift();

#endif /* NIXIE_TUBES_H_ */
