/*
 * DCF77Driver.c
 *
 *  Created on: 01.04.2017
 *      Author: Krzysztof Malicki
 */

#include "nixie_dcf77.h"

// Bit discovery status
#define BIT_NONE				0xFF				// Bit not discovered yet
#define BIT_ERR					0xFE				// Bit set as error
#define BIT_0					0x00				// Discovered 0
#define BIT_1					0x01				// Discovered 1

// Global variables
static uint32_t timeDCF77 = 0;						// DFC77 time in RTC_TR format (24h format)
static uint32_t	dateDCF77 = 0;						// DCF77 date in RTC_DR format

static uint16_t syncState = DCF_NOSYNC;				// Number of synchronized bits in the 60 bit long DFC77 sequence
static uint8_t	syncPending = 0;					// Set to 1 when syncState changes to DCF_SYNC

void dcfResync(){
	if(syncState == DCF_SYNC){
		syncState = DCF_NOSYNC;
		syncPending = 0;
	}
}

void dcfSetRTC(){
	if(syncPending == 0)
		return;										// Not ready for synchronization

	syncPending = 0;								// Clear the flag

	/* Disable the write protection for RTC registers */
	RTC->WPR = 0xCA;
	RTC->WPR = 0x53;

	/* Check if the Initialization mode is set */
	if((RTC->ISR & RTC_ISR_INITF) == 0){
		/* Set the Initialization mode */
		RTC->ISR |= (uint32_t)RTC_ISR_INIT;

	    /* Wait till RTC is in INIT state */
	    while((RTC->ISR & RTC_ISR_INITF) == 0);
	}

	/* Set the RTC_TR (time), RTC_DR (date) and RTC_CR (24h format) registers */
	RTC->TR = (timeDCF77 & 0x007F7F7F);
	RTC->DR = (dateDCF77 & 0x00FFFF3F);
	RTC->CR &= ~((uint32_t)RTC_CR_FMT);

	/* Exit Initialization mode */
	RTC->ISR &= (uint32_t)~RTC_ISR_INIT;

	/* If  RTC_CR_BYPSHAD bit = 0, wait for synchro else this check is not needed */
	if((RTC->CR & RTC_CR_BYPSHAD) == 0){
		/* Clear RSF flag */
		RTC->ISR &= ~((uint32_t)RTC_ISR_RSF);

		/* Wait the registers to be synchronised */
		while((RTC->ISR & RTC_ISR_RSF) == 0);
	}

	/* Enable the write protection for RTC registers */
	RTC->WPR = 0xFF;
}

uint16_t dcfGetSyncState(){
	return syncState;
}

uint16_t dcfSync(){
	/*
	 * DCF77 synchronization routine registers
	 */
	static uint8_t	bitDCF77[60];					// DCF77 sequence bits

	static uint32_t bitRegister = 0;				// DCF77 date and time register
	static uint8_t  bitParity = 0;					// DCF77 even parity check register

	static uint16_t curLevel = 0;					// Current DCF77 radio signal level (High = 1, Low = 0)
	static uint16_t curBit = 0;						// Current bit in a DCF77 minute sequence [0..59]
	static uint8_t* curBitValue = bitDCF77;			// Current bit value (pointer to bitDCF77)

	static uint16_t cntWindow = DCF_PULSE_WINDOW;	// Bit reading window counter (1s = 128 samples)
	static uint16_t cntPulse = 0;					// Pulse length counter (between level changes)
	static uint16_t cntDisruption = 0;				// Pulse disruption length counter (short disruptions are ignored)
	static uint16_t cntErrors = 0;					// Error counter (if too many errors process resets)

	static uint16_t cntShift = 0;					// Shift counter (samples between window start and high pulse start)
	static uint32_t cntShiftSum = 0;				// Shift sum counter (to calculate an average shift)
	static uint16_t cntShiftSmp = 0;				// Shift samples counter (to calculate an average shift)

	static uint8_t  bitStatus = BIT_NONE;			// Current bit values discovery status

	/*
	 * DCF77 synchronization routine body
	 */
	if(syncState == DCF_SYNC) return DCF_SYNC;		// Synchronized with DCF77! Nothing to do

	/*
	 * Sample the DCF77 signal level
	 */
	uint16_t aLevel = ((DCF_GPIO->IDR & DCF_GPIO_PIN) != 0);

	/*
	 * Searching for the beginning of a DCF77 minute sequence (empty bit)
	 */
	if(syncState == DCF_NOSYNC){
		if(aLevel == 0)
			cntPulse++;
		else
			cntPulse = 0;

		if(cntPulse >= DCF_MINMARK_LEN){			// Empty bit (minute mark) was found
			cntWindow = DCF_PULSE_WINDOW;
			cntPulse = 0;
			cntDisruption = 0;

			cntShift = 0;
			cntShiftSum = 0;
			cntShiftSmp = 0;

			cntErrors = 0;

			curLevel = 0;
			curBit = 0;

			bitStatus = BIT_NONE;
			bitRegister = 0;

			curBitValue = bitDCF77;
			for(uint8_t i = 0; i < 60; i++) *(curBitValue++) = BIT_NONE;
			curBitValue = bitDCF77;

			syncState = 0;							// Synchronized with a DCF77 minute sequence! Starting reading bits
			syncPending = 0;
		}

		return syncState;
	}

	/*
	 * Reading a DCF77 sequence bit
	 */
	if(bitStatus == BIT_NONE){
		if(aLevel == curLevel){
			cntPulse++;

			cntPulse += cntDisruption;			// This was a short distortion, ignore it
			cntDisruption = 0;
		} else {
			cntDisruption++;

			if(cntDisruption > DCF_DISTORTION_LEN){
												// This is not a short distortion, but a valid DCF77 signal level change
				if(curLevel == 0){
					cntShift = cntPulse;

					curLevel = 1;
				} else {						// This is the end of a high level pulse. Check if a valid bit was discovered
					if((cntPulse > DCF_PULSE_0_LEN - DCF_DISTORTION_LEN) && (cntPulse < DCF_PULSE_0_LEN + DCF_DISTORTION_LEN)){
						bitStatus = BIT_0;		// Bit 0 discovered

						cntShiftSum += cntShift;
						cntShiftSmp++;
					} else
						if((cntPulse > DCF_PULSE_1_LEN - DCF_DISTORTION_LEN) && (cntPulse < DCF_PULSE_1_LEN + DCF_DISTORTION_LEN)){
							bitStatus = BIT_1;	// Bit 1 discovered

							cntShiftSum += cntShift;
							cntShiftSmp++;
						} else
							bitStatus = BIT_ERR;
				}

				cntPulse = cntDisruption;
				cntDisruption = 0;
			}
		}
	}

	/*
	 * Updating the sequence bits at the end of each second
	 */
	if(--cntWindow == 0){						// At the end of each second ...
		uint16_t newState = syncState;

		switch(bitStatus){						// ... update DCF77 bit in the sequence
			case BIT_0:
				if(*curBitValue == BIT_NONE) newState++;

				*curBitValue = BIT_0;

				break;
			case BIT_1:
				if(*curBitValue == BIT_NONE) newState++;

				*curBitValue = BIT_1;

				break;
			case BIT_NONE:
				if(*curBitValue != BIT_NONE) newState--;

				*curBitValue = BIT_NONE;

				break;
			default:							// BIT_ERR
				if(++cntErrors > DCF_MAX_ERRORS){
												// Resetting the process if too many errors
					cntPulse = 0;

					syncState = DCF_NOSYNC;

					return syncState;
				}
		}

		switch(curBit){							// ... check the DCF77 bit array (sequence) integrity
			case 0:								// Must be 0
				if(*curBitValue == BIT_1){
					newState--;

					*curBitValue = BIT_NONE;
				}

				break;

			case 20:							// Must be 1
				if(*curBitValue == BIT_0){
					newState--;

					*curBitValue = BIT_NONE;
				}

				break;

			case 21: 							// Beginning of minutes
				bitRegister = 0;
			case 29:							// Beginning of hours
			case 36:							// Beginning of date
				if(*curBitValue != BIT_NONE){
					bitParity = *curBitValue;

					bitRegister >>= 1;
					bitRegister |= ((uint32_t)*curBitValue) << 31;
				} else
					bitParity = BIT_NONE;

				break;

			case 28:							// Even parity bit for minutes
			case 35:							// Even parity bit for hours
			case 58:							// Even parity bit for date
				if(curBit == 35)
					timeDCF77 = ((bitRegister & 0xFC000000) >> 10) | \
								((bitRegister & 0x03F80000) >> 11);
												// Calculate time in the RTC_TR format
				else
					if(curBit == 58)
						dateDCF77 = ((bitRegister & 0xFF000000) >> 8) | \
									((bitRegister & 0x00F80000) >> 11) | \
									((bitRegister & 0x00070000) >> 3) | \
									((bitRegister & 0x0000FC00) >> 10);
												// Calculate date in the RTC_DR format

				if((*curBitValue != BIT_NONE) && (*curBitValue != bitParity)){
					newState--;					// Parity check failed

					*curBitValue = BIT_NONE;
				}

				if(*curBitValue == BIT_NONE){
												// Clear the least significant digit of minute if parity check failed
												// This digit must change in the next DFC77 minute sequence
					if(bitDCF77[21] != BIT_NONE){ newState--; bitDCF77[21] = BIT_NONE; }
					if(bitDCF77[22] != BIT_NONE){ newState--; bitDCF77[22] = BIT_NONE; }
					if(bitDCF77[23] != BIT_NONE){ newState--; bitDCF77[23] = BIT_NONE; }
					if(bitDCF77[24] != BIT_NONE){ newState--; bitDCF77[24] = BIT_NONE; }
				}

				break;

			case 59:							// Must be none
				if(*curBitValue != BIT_NONE){
					newState--;

					*curBitValue = BIT_NONE;
				} else {
					if(newState == 59){			// Checking time & date integrity
						if(((timeDCF77 & 0x00300000) <= 0x00200000) && \
						   ((timeDCF77 & 0x000F0000) <= 0x00090000) && \
						   ((timeDCF77 & 0x00007000) <= 0x00005000) && \
						   ((timeDCF77 & 0x00000F00) <= 0x00000900) && \

						   ((dateDCF77 & 0x00F00000) <= 0x00900000) && \
						   ((dateDCF77 & 0x000F0000) <= 0x00090000) && \
						   ((dateDCF77 & 0x00000F00) <= 0x00000900) && \
						   ((dateDCF77 & 0x00000030) <= 0x00000020) && \
						   ((dateDCF77 & 0x0000000F) <= 0x00000009) && \
						   ((dateDCF77 & 0x0000E000) != 0x00000000)){

							uint8_t tmpHour =   ((timeDCF77 & 0x00300000) >> 20) * 10 + ((timeDCF77 & 0x000F0000) >> 16);
							uint8_t tmpMinute = ((timeDCF77 & 0x00007000) >> 12) * 10 + ((timeDCF77 & 0x00000F00) >> 8);
							uint8_t tmpYear =   ((dateDCF77 & 0x00F00000) >> 20) * 10 + ((dateDCF77 & 0x000F0000) >> 16);
							uint8_t tmpMonth =  ((dateDCF77 & 0x00001000) >> 12) * 10 + ((dateDCF77 & 0x00000F00) >> 8);
							uint8_t tmpDay =    ((dateDCF77 & 0x00000030) >>  4) * 10 + ((dateDCF77 & 0x0000000F));

							uint8_t tmpMDays = 31;
							switch(tmpMonth){
								case 2:
									tmpMDays--;
								case 4:
								case 6:
								case 9:
								case 11:
									tmpMDays--;
							}

							// TO DO: Check if day of week matches the date in 21st century

							if((tmpYear >= 17) && (tmpYear <= 99) && \
							   (tmpMonth > 0) && (tmpMonth <= 12) && \
							   (tmpDay > 0) && (tmpDay <= tmpMDays) && \
							   (tmpHour < 24) && \
							   (tmpMinute < 60)){	// We have the correct sequence!
													// Problem in 22nd century! Hope it will work for so long :-)

								newState = DCF_SYNC;

								syncPending = 1;
							}
						}
					}
				}

				break;

			default:
				if(*curBitValue != BIT_NONE){
					if(curBit > 20){			// time and date bits starts from bit 21 in the sequence
						bitRegister >>= 1;
						bitRegister |= ((uint32_t)*curBitValue) << 31;

						if(bitParity != BIT_NONE)
							bitParity ^= *curBitValue;
					}
				} else
					bitParity = BIT_NONE;

				break;
		}

		if(++curBit == 60){
			curBit = 0;							// Next minute starts ...
			curBitValue = bitDCF77;
		} else
			curBitValue++;

		/*
		 * Trimming the discovery window to the DCF77 signal (beginning of the window = beginning of the second).
		 */
		cntWindow = DCF_PULSE_WINDOW;

		if(cntShiftSmp > DCF_MIN_SHIFT_SAMPLES){

			cntShiftSum /= cntShiftSmp;			// Calculate the trim (shift in samples)

			if(cntShiftSum > 1)
				cntWindow += (cntShiftSum - 1);	// Shift the window

			cntShiftSum = 0;
			cntShiftSmp = 0;
		}

		/*
		 * Resetting variables for the next bit
		 */
		cntPulse = 0;
		cntDisruption = 0;
		cntShift = 0;

		bitStatus = BIT_NONE;

		curLevel = 0;

		syncState = newState;
	}

	return syncState;
}
