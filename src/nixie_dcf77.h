/*
 * DCF77Driver.h
 *
 *  Created on: 01.04.2017
 *      Author: Krzysztof Malicki
 */

#ifndef NIXIE_DCF77_H_
#define NIXIE_DCF77_H_

#include "stm32f4xx.h"
#include "nixie_conf.h"

/*
 * Public definitions
 */

#define DCF_NOSYNC				0xFFFF				// DCF77 signal not synchronized yet
#define DCF_SYNC				0x003C				// DCF77 signal synchronized

/*
 * Public methods prototypes
 */

void dcfResync();
void dcfSetRTC();
uint16_t dcfGetSyncState();
uint16_t dcfSync();

#endif /* NIXIE_DCF77_H_ */
