/*
 * nixie_stm32f4xx_init.h
 *
 *  Created on: 02.05.2017
 *      Author: Krzysztof Malicki
 */

#ifndef NIXIE_STM32F4XX_INIT_H_
#define NIXIE_STM32F4XX_INIT_H_

/* Includes ------------------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void nixieInit();

#endif /* NIXIE_STM32F4XX_INIT_H_ */
